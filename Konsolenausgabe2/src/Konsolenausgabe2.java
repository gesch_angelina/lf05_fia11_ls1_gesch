
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.printf( "%6s\n", "**");
		System.out.printf( "%-5s", "*");
		System.out.printf( "%5s\n", "*");
		System.out.printf( "%-5s", "*");
		System.out.printf( "%5s\n", "*");
		System.out.printf( "%6s\n\n", "**");
		
		System.out.printf( "%-5s", "0!");
		System.out.printf( "%-19s", "=");
		System.out.printf( "%2s", "=");
		System.out.printf( "%4s\n", "1");
		System.out.printf( "%-5s", "1!");
		System.out.printf( "%-19s", "= 1");
		System.out.printf( "%2s", "=");
		System.out.printf( "%4s\n", "1");
		System.out.printf( "%-5s", "2!");
		System.out.printf( "%-19s", "= 1 * 2");
		System.out.printf( "%2s", "=");
		System.out.printf( "%4s\n", "2");
		System.out.printf( "%-5s", "3!");
		System.out.printf( "%-19s", "= 1 * 2 * 3");
		System.out.printf( "%2s", "=");
		System.out.printf( "%4s\n", "6");
		System.out.printf( "%-5s", "4!");
		System.out.printf( "%-19s", "= 1 * 2 * 3 * 4");
		System.out.printf( "%2s", "=");
		System.out.printf( "%4s\n", "24");
		System.out.printf( "%-5s", "5!");
		System.out.printf( "%-19s", "= 1 * 2 * 3 * 4 * 5");
		System.out.printf( "%2s", "=");
		System.out.printf( "%4s\n\n", "120");
		
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("%s", "|");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("%s", "------------------------\n");
		System.out.printf( "%+-12d%s%10.2f\n%+-12d%s%10.2f\n%+-12d%s%10.2f\n%+-12d%s%10.2f\n%+-12d%s%10.2f\n" , -20,"|",-28.8889,-10,"|",-23.3333,0,"|",-17.7778,20,"|",-6.6667,30,"|", -1.1111);
		
	}

}
