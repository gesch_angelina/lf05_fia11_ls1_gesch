import java.util.ArrayList;

public class Raumschiff {
	
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<Ladung> Ladungsverzeichnis = new ArrayList<Ladung>();
	private static ArrayList<String>broadcastKommunikator = new ArrayList<String>();
	
	
public Raumschiff () {
		
	}
	
	public Raumschiff (int photonentorpedoAnzahl,int energieversorgungInProzent,
			int zustandSchildeInProzent,int zustandHuelleInProzent,
			int zustandLebenserhaltungssystemeInProzent,
			int anzahlDroiden,String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = schiffsname;
		
		
	}
	
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}	
	
	
	public void addLadung(Ladung neueLadung) {
		this.Ladungsverzeichnis.add(neueLadung);
	}
	public void zustandRaumschiff() {
		System.out.printf("Raumschiff: %s\n", this.schiffsname);
		System.out.printf("Anzahl der Photonentorpedos: %d\n", this.photonentorpedoAnzahl);
		System.out.printf("Energieversorgung: %d %%\n", this.energieversorgungInProzent);
		System.out.printf("Zustand Schilde: %d%%\n", this.schildeInProzent);
		System.out.printf("Zustand H�lle: %d%%\n", this.huelleInProzent);
		System.out.printf("Zustand Lebenserhaltungssysteme: %d%%\n", this.lebenserhaltungssystemeInProzent);
		System.out.printf("Anzahl der Androiden: %d%%\n\n", this.androidenAnzahl);
	}
	public void ladungsverzeichnisAusgeben() {
		int count = 0;
		for(Ladung item : Ladungsverzeichnis) {
			count++;
			System.out.printf("Ladung %d\n", count);
			System.out.printf("Bezeichnung: %s\n", item.getBezeichnung());
			System.out.printf("Menge: %d\n\n", item.getMenge());
			
		}
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(r.getPhotonentorpedoAnzahl()<0) {
			r.setPhotonentorpedoAnzahl(r.getPhotonentorpedoAnzahl() - 1);
		}else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(r.getEnergieversorgungInProzent() > 50) {
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);
		}else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}
	public ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator;
	}
	
	private void treffer(Raumschiff r) {
		this.schildeInProzent -= 50;
		if(this.schildeInProzent <= 0) {
			this.huelleInProzent -= 50;
			this.energieversorgungInProzent -= 50;
			if(this.huelleInProzent <= 0) {
				nachrichtAnAlle("-=*Click*=-");
			}
		}
	}
}
