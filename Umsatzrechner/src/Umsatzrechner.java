import java.util.Scanner;
public class Umsatzrechner {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		char Schleife = 'n';
		do {
			Schleife = 'n';
			boolean Fehler = false;
			float jahresumsatz = 0;
			float umsaetze [] = new float [5];

			for (int i = 1; i <= 4; i++) {
				do {
					try {
						Fehler = false;
						System.out.print("Geben Sie den Umsatz von Quartal "+ i + " ein (ohne �): ");
						umsaetze [i] = eingabe.nextFloat();
					}catch(Exception e){
						Fehler = true;
						eingabe.next();
						System.out.println("Eingabefehler. Versuche erneut.");
					}
				}while (Fehler);
			}

			for (int i = 1; i <=4; i++) {
				jahresumsatz = jahresumsatz + umsaetze [i];
			}

			System.out.println("Der Jahresumsatz betr�gt "+ jahresumsatz + "�.");

			for (int i = 1; i <= 4; i++) {
				umsaetze [i] = (umsaetze [i]/jahresumsatz)*100;
				System.out.println(umsaetze [i] + "% davon sind aus dem " + i + ". Quartal.");
			}

			do {
				System.out.print("M�chten Sie nochmal berechnen? (j = ja; n = nein): ");
				Schleife = eingabe.next().charAt(0);
			}while((Schleife != 'j') && (Schleife != 'n'));

		}while(Schleife == 'j');

		System.out.println("Programmende.");
		eingabe.close();


	}

}
