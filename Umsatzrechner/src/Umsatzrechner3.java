import java.util.Scanner;
public class Umsatzrechner3 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		char Schleife = 'n';
		do {
			Schleife = 'n';
			boolean Fehler = false;
			float jahresumsatz = 0;
			float umsaetze [] = new float [12];
			String monate [] = {"Januar","Februar","M�rz","April","Mai", "Juni", "Juli", "August", "September", "Oktober", "November","Dezember"};

			for (int i = 0; i < 12; i++) {
				do {
					try {
						Fehler = false;
						System.out.printf("Geben Sie den Umsatz von %s ein (ohne �): ", monate [i]);
						umsaetze [i] = eingabe.nextFloat();
					}catch(Exception e){
						Fehler = true;
						eingabe.next();
						System.out.println("Eingabefehler. Versuche erneut.");
					}
				}while (Fehler);
			}

			for (int i = 0; i <12; i++) {
				jahresumsatz = jahresumsatz + umsaetze [i];
			}

			System.out.println("Der Jahresumsatz betr�gt "+ jahresumsatz + "�.");

			for (int i = 0; i < 12; i++) {
				umsaetze [i] = (umsaetze [i]/jahresumsatz)*100;
				System.out.printf("%6.2f%% davon sind von %s.\n", umsaetze [i], monate [i]);
			}

			do {
				System.out.print("M�chten Sie nochmal berechnen? (j = ja; n = nein): ");
				Schleife = eingabe.next().charAt(0);
			}while((Schleife != 'j') && (Schleife != 'n'));

		}while(Schleife == 'j');

		System.out.println("Programmende.");
		eingabe.close();
	}

}
