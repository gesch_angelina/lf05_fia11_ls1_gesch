import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		 String artikel = liesString(myScanner);

		int anzahl = liesInt(myScanner);

		double preis = liesDouble(myScanner);
		double mwst = liesDouble2(myScanner);

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis,mwst);

		// Ausgeben

		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	public static String liesString(Scanner myScanner) {
		System.out.println("was m�chten Sie bestellen?");
		return myScanner.next();
	}
	public static int liesInt(Scanner myScanner) {
		System.out.println("Geben Sie die Anzahl ein:");
		return myScanner.nextInt();
	}
	public static double liesDouble(Scanner myScanner) {
		System.out.println("Geben Sie den Nettopreis ein:");
		return myScanner.nextDouble();
	}
	public static double liesDouble2(Scanner myScanner) {
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		return myScanner.nextDouble();
	}
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
	return anzahl * preis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
	return nettogesamtpreis * (1 + mwst / 100);
	}
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
