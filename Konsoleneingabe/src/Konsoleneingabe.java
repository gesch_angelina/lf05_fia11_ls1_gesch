import java.util.Scanner;

public class Konsoleneingabe {

	public static void main(String[] args) {
		
		String name;
		int alter;
		String lieblingsfarbe;
		float groesse;
		char geschlecht;
		String lieblingstier;
		
		Scanner eingabe = new Scanner (System.in);
		
		System.out.print("Hallo! Geben Sie Ihren Namen ein: ");
		name = eingabe.next();
		System.out.println("Hallo " + name + "!");
		
		System.out.print("Geben Sie Ihr Alter ein: ");
		alter = eingabe.nextInt();
		
		System.out.print("Geben Sie Ihre Lieblingsfarbe ein: ");
		lieblingsfarbe = eingabe.next();
		
		System.out.print("Geben Sie Ihre Gr��e ein: ");
		groesse = eingabe.nextFloat();
		
		System.out.print("Geben Sie Ihr Geschlecht ein (m/w/d): ");
		geschlecht = eingabe.next().charAt(0);
		
		System.out.print("Geben Sie Ihr Lieblingstier ein: ");
		lieblingstier = eingabe.next();
		
		
		
		 
		System.out.println("Name: " + name);
		System.out.println("Alter: " + alter);
		System.out.println("Lieblingsfarbe: " + lieblingsfarbe);
		System.out.println("Gr��e: " + groesse);
		System.out.println("Geschlecht: " + geschlecht);
		System.out.println("Lieblingstier: " + lieblingstier);


		eingabe.close();
		
	}

}
