import java.util.Scanner;

public class RomZahlen_sortieren {

	static boolean sortierfehler = false;
	
	public static void main (String[] args) {
	
		Scanner eingabe = new Scanner (System.in);
		String romZahl1 = "";
		String romZahl2 = "";
		String romZahl3 = "";
		String romZahlen_sortiert [] = new String[3];
		
		System.out.println("Dieses Programm sortiert r�mische Zahlen in aufsteigender Reihenfolge nach der Gr��e.");
		
		System.out.print("Bitte geben Sie eine r�mische Zahl ein: ");
		romZahl1 = nutzereingabe(eingabe);
		int dezRom1 = umrechnen(romZahl1);
		
		System.out.print("Bitte geben Sie noch eine r�mische Zahl ein: ");
		romZahl2 = nutzereingabe(eingabe);	
		int dezRom2 = umrechnen(romZahl2);
		
		System.out.print("Bitte geben Sie noch eine letzte r�mische Zahl ein: ");
		romZahl3 = nutzereingabe(eingabe);		
		int dezRom3 = umrechnen(romZahl3);
		
		int dezRom_sortiert[] = sort(dezRom1, dezRom2, dezRom3);
		
	for(int i=0; dezRom_sortiert.length > i; i++) {
			
			if (dezRom_sortiert[i] == dezRom1){
				 romZahlen_sortiert[i] = romZahl1;
			}else if (dezRom_sortiert[i] == dezRom2) {
				 romZahlen_sortiert[i] = romZahl2;
			}else if(dezRom_sortiert[i] == dezRom3) {
				 romZahlen_sortiert[i] = romZahl3;
			}
			
		}
	
		
		if (sortierfehler) {
			System.out.println("Sortierfehler.");
		}else {
			System.out.printf("\nZeichen werden sortiert");
			for (int i = 0; i < 3; i++)
	        {
				try {
		 			Thread.sleep(1000);
		 		} catch (InterruptedException e) {
		 			e.printStackTrace();
		 		}
	           System.out.print(".");
	           
	        }
			
			System.out.printf(" \n \n");
			System.out.printf("%s, %s, %s\n", romZahlen_sortiert[0], romZahlen_sortiert[1], romZahlen_sortiert[2]);
			System.out.printf("%s, %s, %s\n", dezRom_sortiert[0], dezRom_sortiert[1], dezRom_sortiert[2]);
		}
		
		eingabe.close();
	}
	
	public static int[] sort (int eingabe_1, int eingabe_2, int eingabe_3) {
		int zeichen_sortiert [] = new int [3];
		if (eingabe_1 > eingabe_2) {
			
			if(eingabe_1 > eingabe_3) {
				
				if(eingabe_2 > eingabe_3) {
					zeichen_sortiert[0] = eingabe_3;
					zeichen_sortiert[1] = eingabe_2;
					zeichen_sortiert[2] = eingabe_1;
				}else if(eingabe_2 < eingabe_3) {
					zeichen_sortiert[0] = eingabe_2;
					zeichen_sortiert[1] = eingabe_3;
					zeichen_sortiert[2] = eingabe_1;
				}else {
					sortierfehler = true;
				}
				
			}else if(eingabe_1 < eingabe_3) {
				zeichen_sortiert[0] = eingabe_2;
				zeichen_sortiert[1] = eingabe_1;
				zeichen_sortiert[2] = eingabe_3;
			}else {
				sortierfehler = true;
			}
			
	
		}else if (eingabe_1 < eingabe_2) {
			
			if(eingabe_1 < eingabe_3) {
				if(eingabe_3 > eingabe_2) {
					zeichen_sortiert[0] = eingabe_1;
					zeichen_sortiert[1] = eingabe_2;
					zeichen_sortiert[2] = eingabe_3;
				}else if(eingabe_3 < eingabe_2) {
					zeichen_sortiert[0] = eingabe_1;
					zeichen_sortiert[1] = eingabe_3;
					zeichen_sortiert[2] = eingabe_2;
				}else {
					sortierfehler = true;
				}
			}else if (eingabe_1 > eingabe_3) {
				zeichen_sortiert[0] = eingabe_3;
				zeichen_sortiert[1] = eingabe_1;
				zeichen_sortiert[2] = eingabe_2;
			}else {
				sortierfehler = true;
			}
			
		}else {
			sortierfehler = true;
	}
		return zeichen_sortiert;

	}
	
	public static int umrechnen (String roemische_Zahl) {
		int dezimalzahl = 0;
		for (int i = 0; i < roemische_Zahl.length(); i++){
			switch(roemische_Zahl.charAt(i)) {
			case 'M':
				dezimalzahl += 1000;
				break;
			case 'D':
				dezimalzahl += 500;
				break;
			case 'C':
				if(i+1 < roemische_Zahl.length()) {
				if (roemische_Zahl.charAt(i+1) == 'D' || roemische_Zahl.charAt(i+1) == 'M'){
					dezimalzahl -= 100;
					break;
				}
				}				
					dezimalzahl += 100;
					break;				
			case 'L':
				dezimalzahl += 50;
				break;
			case 'X':
				if(i+1 < roemische_Zahl.length()) {
				if (roemische_Zahl.charAt(i+1) == 'L' || roemische_Zahl.charAt(i+1) == 'C'){
					dezimalzahl -= 10;
					break;
				}
				}					
					dezimalzahl += 10;
					break;				
			case 'V':
				dezimalzahl += 5;
				break;
			case 'I':
				if(i+1 < roemische_Zahl.length()) {
					if (roemische_Zahl.charAt(i+1) == 'V' || roemische_Zahl.charAt(i+1) == 'X'){
						dezimalzahl -= 1;
						break;
					}
				}					
						dezimalzahl += 1;
						break;				
			default:
					dezimalzahl=0;
				}
			}
			
		if (dezimalzahl==0) {
			System.out.println("Fehler beim Umrechnen.");
			System.exit(0);
		}
		
			return dezimalzahl;
		}
	
	public static boolean check_rom (String roemische_Zahl){

		//Das I, X,C und M d�rfen nicht mehr als 3 Mal hintereinander vorkommen
		if (roemische_Zahl.contains( "IIII") || roemische_Zahl.contains("XXXX") || roemische_Zahl.contains("CCCC")|| roemische_Zahl.contains("MMMM"))
		{
			return false;
		}
		
		//F�r jedes Zeichen gucken, ob der Vorg�nger korrekt ist
		for(int i = 0; i < roemische_Zahl.length(); i++) {
			switch (roemische_Zahl.charAt(i)) {
			case 'I':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':	
						break;
					case 'V':
						break;
					case 'X':
						break;
					case 'L':
						break;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'V':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						break;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						break;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'X':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						break;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						break;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'L':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'C':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'D':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						return false;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'M':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						return false;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						return false;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			default:
				return false;
			}
		}

		
		return true;
	}
	
	public static String nutzereingabe (Scanner eingabe) {
		String romZahl = eingabe.next().toUpperCase();
		while (!check_rom(romZahl)) {
			System.out.println("ung�ltige Eingabe");
			System.out.print("Versuchen Sie es erneut: ");
			romZahl = eingabe.next().toUpperCase();
		}
		return romZahl;
	}

	
}



