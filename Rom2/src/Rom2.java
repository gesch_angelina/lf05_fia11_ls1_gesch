import java.util.Scanner;
public class Rom2 {

	public static void main(String[] args) {
		//Ben�tigte Variablen deklarieren
		Scanner eingabe = new Scanner (System.in);
		String roemische_Zahl = "";
		int dezimalzahl = 0;
		
		//Eingabe
		System.out.print("Geben Sie ein r�misches Zahlzeichen ein: ");
		roemische_Zahl = eingabe.next();
		roemische_Zahl.toUpperCase();
		
		//Checken ob roemische Zahl korrekt eingegeben wurde, wenn ja Funktion zum Umrechnen aufrufen
		if(check_rome(roemische_Zahl)) {
			
				dezimalzahl = umrechnen(roemische_Zahl);
		}
		
		//Ausgabe
			if (dezimalzahl != 0 && dezimalzahl <= 3999) {
				System.out.printf("Der Dezimalwert des Zahlzeichens %s betr�gt %d.", roemische_Zahl, dezimalzahl);
			}else {
			System.out.println("Ihre Eingabe ist ung�ltig.");
		}		
		eingabe.close();
	}
	
	
	//Funktion zur Pr�fung ob r�mische Zahl korrekt ist
	public static boolean check_rome (String roemische_Zahl){

		//Das I, X,C und M d�rfen nicht mehr als 3 Mal hintereinander vorkommen
        if (roemische_Zahl.contains( "IIII") || roemische_Zahl.contains("XXXX") || roemische_Zahl.contains("CCCC")|| roemische_Zahl.contains("MMMM") ||
                // und ein Zahlzeichen darf nicht auf eine Gruppe von Ziffern in subtraktiver Schreibweise des gleichen Bereichswertes folgen.
                roemische_Zahl.contains("IXI") || roemische_Zahl.contains("IVI") || roemische_Zahl.contains("XLX") || roemische_Zahl.contains("XCX") || roemische_Zahl.contains("CDC") || roemische_Zahl.contains("CMC") ||
                //Eine Ziffer von niedrigerem Wert, I ( = 1), kann sich nicht vor einer Gruppe von Ziffern in subtraktiven Notation.
                roemische_Zahl.contains("IIV") || roemische_Zahl.contains("IIX") || roemische_Zahl.contains("XXL") || roemische_Zahl.contains("XXC") || roemische_Zahl.contains("CCD") || roemische_Zahl.contains("CCM"))
        {
            return false;
        }
		
		//F�r jedes Zeichen gucken, ob der Vorg�nger korrekt ist
		for(int i = 0; i < roemische_Zahl.length(); i++) {
			switch (roemische_Zahl.charAt(i)) {
			case 'I':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':	
						break;
					case 'V':
						break;
					case 'X':
						break;
					case 'L':
						break;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'V':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						break;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						break;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'X':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						break;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						break;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'L':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'C':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						break;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'D':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						break;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						return false;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			case 'M':
				if(0 <= i-1) {
					switch (roemische_Zahl.charAt(i-1)) {
					case 'I':
						return false;
					case 'V':
						return false;
					case 'X':
						return false;
					case 'L':
						return false;
					case 'C':
						break;
					case 'D':
						return false;
					case 'M':
						break;
					default:
						return false;
					}
				}
				break;
			default:
				return false;
			}
		}

		
		return true;
	}
	
	
	//Funktion um die Zahl ins dezimale umzurechnen
	public static int umrechnen (String roemische_Zahl) {
		int dezimalzahl = 0;
		for (int i = 0; i < roemische_Zahl.length(); i++){
			switch(roemische_Zahl.charAt(i)) {
			case 'M':
				dezimalzahl += 1000;
				break;
			case 'D':
				dezimalzahl += 500;
				break;
			case 'C':
				if(i+1 < roemische_Zahl.length()) {
				if (roemische_Zahl.charAt(i+1) == 'D' || roemische_Zahl.charAt(i+1) == 'M'){
					dezimalzahl -= 100;
					break;
				}
				}				
					dezimalzahl += 100;
					break;				
			case 'L':
				dezimalzahl += 50;
				break;
			case 'X':
				if(i+1 < roemische_Zahl.length()) {
				if (roemische_Zahl.charAt(i+1) == 'L' || roemische_Zahl.charAt(i+1) == 'C'){
					dezimalzahl -= 10;
					break;
				}
				}					
					dezimalzahl += 10;
					break;				
			case 'V':
				dezimalzahl += 5;
				break;
			case 'I':
				if(i+1 < roemische_Zahl.length()) {
					if (roemische_Zahl.charAt(i+1) == 'V' || roemische_Zahl.charAt(i+1) == 'X'){
						dezimalzahl -= 1;
						break;
					}
				}					
						dezimalzahl += 1;
						break;				
			default:
				return 0;
			}
			
		}
	
	return dezimalzahl;

	}
	

}

