import java.util.Scanner;
public class Rom {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner (System.in);
		char zahlzeichen = ' ';
		int dezimalzahl = 0;
		boolean falscher_wert = false;

		System.out.print("Geben Sie ein r�misches Zahlzeichen ein: ");
		zahlzeichen = eingabe.next().charAt(0);
		
		switch(zahlzeichen) {
		case 'I':
			dezimalzahl = 1;
			break;
		case 'V':
			dezimalzahl = 5;
			break;
		case 'X':
			dezimalzahl = 10;
			break;
		case 'L':
			dezimalzahl = 50;
			break;
		case 'C':
			dezimalzahl = 100;
			break;
		case 'D':
			dezimalzahl = 500;
			break;
		case 'M':
			dezimalzahl = 1000;
			break;
		default:
			falscher_wert = true;
			System.out.println("Ihre Eingabe ist ung�ltig.");
			break;
		}
		
		if (!falscher_wert) {
			System.out.printf("Der Dezimalwert des Zahlzeichens %s betr�gt %d.", zahlzeichen, dezimalzahl);
		}
		
		
		eingabe.close();
	}

}
