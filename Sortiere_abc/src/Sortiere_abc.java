import java.util.Scanner;


public class Sortiere_abc {
	
	static boolean sortierfehler = false;
	public static void main(String[] args){
		Scanner eingabe = new Scanner(System.in);
		
		
		//Eingabe
		System.out.print("Geben Sie ein Zeichen ein: ");
		char eingabe_1 = eingabe.next().charAt(0);
		System.out.print("Geben Sie noch ein Zeichen ein: ");
		char eingabe_2 = eingabe.next().charAt(0);
		System.out.print("Geben Sie ein letztes Zeichen ein: ");
		char eingabe_3 = eingabe.next().charAt(0);
		
		System.out.print("Zeichen werden sortiert");
		for (int i = 0; i < 3; i++)
        {
			try {
	 			Thread.sleep(1000);
	 		} catch (InterruptedException e) {
	 			e.printStackTrace();
	 		}
           System.out.print(".");
           
        }
		
		System.out.printf(" \n");
		
		//Verarbeitung
		char zeichen_sortiert[] = sort(eingabe_1, eingabe_2, eingabe_3);
		
		//Ausgabe
		if (sortierfehler) {
			System.out.println("Sortierfehler.");
		}else {
			System.out.printf("%s, %s, %s", zeichen_sortiert[0], zeichen_sortiert[1], zeichen_sortiert[2]);
		}
		
	
		
		eingabe.close();
	}
	
	public static char[] sort (char eingabe_1, char eingabe_2, char eingabe_3) {
		char zeichen_sortiert [] = new char [3];
		if (eingabe_1 > eingabe_2) {
			
			if(eingabe_1 > eingabe_3) {
				
				if(eingabe_2 > eingabe_3) {
					zeichen_sortiert[0] = eingabe_3;
					zeichen_sortiert[1] = eingabe_2;
					zeichen_sortiert[2] = eingabe_1;
				}else if(eingabe_2 < eingabe_3) {
					zeichen_sortiert[0] = eingabe_2;
					zeichen_sortiert[1] = eingabe_3;
					zeichen_sortiert[2] = eingabe_1;
				}else {
					sortierfehler = true;
				}
				
			}else if(eingabe_1 < eingabe_3) {
				zeichen_sortiert[0] = eingabe_2;
				zeichen_sortiert[1] = eingabe_1;
				zeichen_sortiert[2] = eingabe_3;
			}else {
				sortierfehler = true;
			}
			
	
		}else if (eingabe_1 < eingabe_2) {
			
			if(eingabe_1 < eingabe_3) {
				if(eingabe_3 > eingabe_2) {
					zeichen_sortiert[0] = eingabe_1;
					zeichen_sortiert[1] = eingabe_2;
					zeichen_sortiert[2] = eingabe_3;
				}else if(eingabe_3 < eingabe_2) {
					zeichen_sortiert[0] = eingabe_1;
					zeichen_sortiert[1] = eingabe_3;
					zeichen_sortiert[2] = eingabe_2;
				}
			}else if (eingabe_1 > eingabe_3) {
				zeichen_sortiert[0] = eingabe_3;
				zeichen_sortiert[1] = eingabe_1;
				zeichen_sortiert[2] = eingabe_2;
			}else {
				sortierfehler = true;
			}
			
		}else {
			sortierfehler = true;
	}
		
		return zeichen_sortiert;

	}
}
