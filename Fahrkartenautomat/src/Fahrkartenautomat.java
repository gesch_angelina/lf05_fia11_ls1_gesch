﻿import java.util.Scanner;

class Fahrkartenautomat
{	
    public static void main(String[] args)
    {
    	
    	while(true) {
    		Scanner tastatur = new Scanner(System.in);
    	      
    	       double zuZahlenderBetrag; 
    	       double eingezahlterGesamtbetrag;

    	       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
    	       // Geldeinwurf
    	       // -----------
    	       eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);

    	       // Fahrscheinausgabe
    	       // -----------------
    	       fahrkartenAusgeben();

    	       // Rückgeldberechnung und -Ausgabe
    	       // -------------------------------
    	       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    	       
    	       System.out.println("Die nächste Bestellung wird vorbereitet.");
    	       warte(100);
    	       System.out.print("\n\n");
    	}
    }
    
    public static float fahrkartenbestellungErfassen(Scanner tastatur) {
    	String bezeichnung [] = {"","Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzeilfahrschein Berlin ABC","Kurzstrecke",
    							 "Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC",
    							 "Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC" };
    	double preise [] = {0,2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	boolean fehler = false;
    	float zuZahlenderBetrag = 0.0f;
    	byte anzahlTickets = 0;
    	byte fahrkarteTarif = 0;
    	double ticketpreis = 0;
        		
        	System.out.println("Fahrkartenbestellvorgang:\n" +
        						"=========================");
      
        	while(true) {
        		
        		do {
        			
        	System.out.printf("\nWählen Sie ihre Wunschfahrkarte für Berlin aus:\n");
        			fehler = false;
        	for(int i=1;i<bezeichnung.length;i++){
        		System.out.printf("%s [%.2f EUR] (%d)\n", bezeichnung[i], preise[i],i);
        	}
        	System.out.printf("Bezahlen (%d)\n\n", bezeichnung.length);
        	
        	System.out.print("Ihre Wahl: ");
        	fahrkarteTarif = tastatur.nextByte();
        	
        	if(fahrkarteTarif>0 && fahrkarteTarif<bezeichnung.length) {
        		ticketpreis = preise[fahrkarteTarif];	
        	}else if(fahrkarteTarif == bezeichnung.length){
        		return zuZahlenderBetrag;
        	}else {
        		System.out.println("Ihre Eingabe war falsch. Bitte versuchen Sie es erneut.\n");
        		fehler = true;
        	}
        	}while(fehler);

            System.out.print("Anzahl Tickets: ");
            anzahlTickets = tastatur.nextByte();
            if (anzahlTickets > 10 || anzahlTickets < 1) {
            	anzahlTickets = 1;
            	System.out.println("Sie haben eine ungültige Anzahl an Tickets eingegeben, die Anzahl wurde auf 1 gesetzt.");
            }
            
            zuZahlenderBetrag += ticketpreis*anzahlTickets;
            System.out.printf("Zwischensumme: %.2f €\n\n", zuZahlenderBetrag);
            warte(100);
        	}
    	
        
    }
    public static float fahrkartenBezahlen (Scanner tastatur, double zuZahlenderBetrag) {
    	float eingezahlterGesamtbetrag = 0.0f;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   float eingeworfeneMünze = tastatur.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
		return eingezahlterGesamtbetrag;
    }
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        warte(250);
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  muenzeAusgeben(1, "EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  muenzeAusgeben(50, "CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  muenzeAusgeben(20, "CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  muenzeAusgeben(10, "CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  muenzeAusgeben(5, "CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
    
    static void warte(int millisekunde) {
    	for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
    }
    static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
}