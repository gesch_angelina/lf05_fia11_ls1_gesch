import java.util.Scanner;

public class Multiplikation {

    public static void main(String[] args) {
       
        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        Scanner myScanner = new Scanner(System.in);
       
        //1.Programmhinweis
       programmhinweis();

        //4.Eingabe
        zahl1 = eingabe(myScanner, 1);
        zahl2 = eingabe(myScanner,2);

        //3.Verarbeitung
        erg = verarbeitung(zahl1,zahl2);

        //2.Ausgabe   
        ausgabe(zahl1,zahl2, erg);
        
    }
    
    public static void programmhinweis() {
    	 System.out.println("Hinweis: ");
         System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
    }
    
    public static void ausgabe(double zahl1, double zahl2, double erg) {
    	System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
    }
    
    public static double verarbeitung(double zahl1, double zahl2) {
    	return zahl1 * zahl2;
    }
    
    public static double eingabe (Scanner myScanner, int abfragenummer){
    	System.out.print(abfragenummer +". Zahl: ");
        return myScanner.nextDouble();

    }
}

//Antworttext
