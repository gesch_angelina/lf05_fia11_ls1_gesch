import java.util.Scanner;

public class Schleifen {
	

	public static void main(String[] args) {


		Scanner eingabe = new Scanner(System.in);
		
		
		int ende = 0;
		int start = 0;
		long quersumme = 0;
		
		while((ende != -1) && (start != -1)){			
			
		System.out.print("Startzahl:");
		start = eingabe.nextInt();
		
		System.out.print("Endzahl:");
		ende = eingabe.nextInt();
		
		quersumme = 0;
		
		for (int zaehler = start; zaehler <= ende; zaehler++)
		{
			quersumme = quersumme + zaehler;
		}
		
		System.out.println("Die Quersumme ist: " + quersumme);
				
		}
		
		System.out.println("Programm Ende.");
		
		eingabe.close();
	}
}
