import java.util.Scanner;
public class Schaltjahr {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		int eingabe_Jahr;
		
		//Eingabe
		System.out.print("Geben Sie ein Jahr ein: ");
		eingabe_Jahr = eingabe.nextInt();
		
		//Regel 1: Ein Schaltjahr ist alle vier Jahre
		if((eingabe_Jahr % 4) == 0) {
			//Regeln 2&3 wurden erst 1582 eingeführt
			if(eingabe_Jahr >= 1582) {
				
				//Regel 2: alle hundert Jahre nicht
			if((eingabe_Jahr % 100) == 0) {
				
				//Regel 3: und alle vierhundert Jahre doch
				if((eingabe_Jahr%400) == 0) {
					System.out.println(eingabe_Jahr + " ist ein Schaltjahr.");
				}else {
					System.out.println(eingabe_Jahr + " ist kein Schaltjahr.");
				}
			
			//else von Regel 2
			}else {
				System.out.println(eingabe_Jahr + " ist ein Schaltjahr.");
			}
			}
			
		//else von Regel 1
		}else {
			System.out.println(eingabe_Jahr + " ist kein Schaltjahr.");
		}
		
		eingabe.close();
	}
}
